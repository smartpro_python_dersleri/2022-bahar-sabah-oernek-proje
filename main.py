from typing import NoReturn
from getpass import getpass

from database import DataBase
from libs import get_task_input


def main() -> NoReturn:
    db = DataBase('db.json')

    while True:
        task = get_task_input(['list', 'add', 'login', 'quit'])

        match task:
            case 'list':
                for i in db.list_users():
                    print(i)
            case 'add':
                user = db.add_user()
                print(f'"{user}" başarı ile oluşturuldu!')
            case 'login':
                username = input('Kullanıcı Adı: ')
                password = getpass('Parola: ')
                if db.check_user_login(username, password):
                    print(f'Hoşgeldin "{username}"')
                else:
                    print('Kullanıcı adı ya da parolanız hatalıdır!!!')
            case 'quit':
                print('Güle güle sana dostum!!!')
                exit()


if __name__ == "__main__":
    main()
