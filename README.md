# Kullanıcı Kaydı

## Bu projeden beklentiler
* Kullanıcıdan aşağıdaki bilgileri alarak kayıt oluşturma
    * Kullanıcı adı
    * Tam ad
    * Parola
* Kullanıcılar bir veritabanına kaydedilecektir.
* Parola kaydedilirken hash'i alınarak kaydedilmelidir.
* Kullanıcının giriş yapması için kullanıcı adı ve parola sorulacaktır
* Parola, hash'li haliyle kıyaslanarak kontrol edilecektir.
* Eğer kullanıcı adı ve parola tutarsa True, tutmazsa False dönecektir.
