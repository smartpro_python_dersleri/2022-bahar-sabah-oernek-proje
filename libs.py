from hashlib import sha256
from getpass import getpass
from string import ascii_letters, punctuation


def get_password_hash(password: str) -> str:
    hashed_pass = sha256(f'{password}_not_so_secret'.encode('utf-8')).hexdigest()
    return hashed_pass


def is_password_valid(password: str) -> bool:
    if len(password) < 8:
        return False
    has_digit = any(x.isdigit() for x in password)
    has_letter = any(x in ascii_letters for x in password)
    has_punct = any(x in punctuation for x in password)
    return has_digit and has_letter and has_punct


def get_password_input() -> str:
    while True:
        user_input = getpass('Parola: ')
        if is_password_valid(user_input):
            return user_input
        print('Parolanız en az 8 karakterden oluşmalıdır ve harf, sayı ve özel karakter içermelidir!')


def get_task_input(tasks: list[str]) -> str:
    while True:
        user_input = input('Lütfen bir işlem seçiniz: ' + ', '.join(tasks) + ': ')
        if user_input.lower() in tasks:
            return user_input.lower()
        print('Lütfen geçerli bir işlem seçiniz!!!!')
