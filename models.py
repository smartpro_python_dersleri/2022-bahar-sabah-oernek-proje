class User:
    __slots__ = ('username', 'fullname', 'password')

    def __init__(self, username: str, fullname: str, password: str) -> None:
        self.username = username
        self.fullname = fullname
        self.password = password

    def __str__(self) -> str:
        return self.username

    def to_json(self) -> dict[str, str]:
        return {
            'username': self.username,
            'fullname': self.fullname,
            'password': self.password,
        }
