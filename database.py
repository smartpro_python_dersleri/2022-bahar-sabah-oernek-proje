from json import loads, dumps, JSONDecodeError

from models import User
from libs import get_password_input, get_password_hash


class DataBase:
    def __init__(self, db_path: str) -> None:
        self.db_path = db_path
        self.read_from_db()

    def read_from_db(self) -> None:
        try:
            with open(self.db_path, 'r') as f:
                raw = loads(f.read())
                output = []
                for i in raw:
                    output.append(
                        User(
                            username=i['username'],
                            fullname=i['fullname'],
                            password=i['password']
                        )
                    )
                self.objects = output
        except FileNotFoundError:
            with open(self.db_path, 'w') as f:
                f.write(dumps([]))
            self.objects = []
        except JSONDecodeError:
            print('Veritabanını okurken bir hata oldu. Lütfen kontrol edin!')
            exit()

    def write_to_db(self) -> None:
        try:
            with open(self.db_path, 'w') as f:
                as_dict = [x.to_json() for x in self.objects]
                f.write(dumps(as_dict, indent=2))
            self.read_from_db()
        except Exception as e:
            print(e, e.args, e.__class__)

    def list_users(self) -> list[str]:
        return [f'{x.username} - {x.fullname}' for x in self.objects]

    def is_username_exists(self, username: str) -> bool:
        for i in self.objects:
            if i.username == username:
                return True
        return False

    def add_user(self) -> User:
        while True:
            username = input('Kullanıcı Adı: ')
            if not self.is_username_exists(username):
                break
        fullname = input('Tam Adı: ')
        password = get_password_hash(get_password_input())
        user = User(username, fullname, password)

        self.objects.append(user)
        self.write_to_db()
        return user

    def check_user_login(self, username: str, password: str) -> bool:
        if self.is_username_exists(username):
            user = [x for x in self.objects if x.username == username][0]
            if user.password == get_password_hash(password):
                return True
        return False
